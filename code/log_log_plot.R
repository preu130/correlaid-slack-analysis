library(ggplot2)
library(readr)
library(dplyr)
ec <- read_rds("data/emoji_counts_correlaid.rds")

ec <- ec %>% 
  arrange(desc(count)) %>% 
  mutate(rank = 1:n())

ggplot(ec, aes(x = log(rank), y = log(count)))+
  geom_point()+
  ggsave("figures/log_log_plot.png")
