Emoji reactions in the CorrelAid Slack
================

Intro
-----

[CorrelAid](https://correlaid.org/en/) is a data-for-good network of over 450 young data scientists who help social organizations by analyzing their data. To make our data-for-good projects possible, our *Core Team* is working on the organizational side of things using [Slack](https://slack.com/). Slack is a communication tool that allows you to organize your conversations by topics in so-called channels where every member can then post messages. But most importantly, members can *react* to others' messages using emojis. This is a neat way of quickly expressing your opinion. Being an emotional and talkative, but also always in-a-rush kind of bunch, this features is one we like and use a lot.

Soo.. Slack has an [API](api.slack.com/). And what's the most natural thing to do if you're a data scientist writing your Master's thesis? Yeah. Sure. Let's procrastinate a bit and pull this data.

Using the mentioned Slack API, I was able to pull out ~5,800 messages in public channels out of approximately 20,000 we have sent in total according to the Slack Analytics tool. I did not have the time to check where the ~~y~~huge difference come from - I doubt that 3/4 of our messages are sent in private conversations and channels which are not covered by those data - so if anyone can enlighten me, please drop me a message.

I now turn to some technical details/procedures that will allow you to conduct an analysis of your own Slack emoji reactions.

How can I run this?
-------------------

For privacy reasons, the only data uploaded here are the data corresponding to the table down below. That's not terribly exciting. If you want to generate a similar table for your own Slack and/or do some more analyses on your public channel messages, you have to do the following:

-   create your own Slack App
    -   with **Administrator rights**, go to <https://api.slack.com/apps>:
    -   click "Create new App" and enter a name for your app
    -   under "Add features and functionality", go to "Permissions"" and grant your App the necessary permissions, namely `channels:read` and `channels:history`. We will need those to first get the list of our channels ([API method `channels.list`](https://api.slack.com/methods/channels.list)) and fetch the history for each channel ([API method `channels.history`](https://api.slack.com/methods/channels.history))
    -   go back and click "Install your app to your team". Go ahead and add your app to your team. Unfortunately, your app will occupy one of your "app spaces" like any normal app would. So make sure you still have enough "app spaces".
    -   now you should be able to go to "Oauth & Permissions" under "Features" on the left sidebar and request an OAuth Access Token. Copy the token into a text file called `slack_key.txt` and put it in your `data` folder.
-   make sure you have all packages installed. See the header of `code/get_emoji_data.R` for all required packages (mostly `tidyverse`).

To the results!
---------------

After this boring section, let's now have a look at the results: Here's the list of emojis that is used on the CorrelAid Slack -- in decreasing order of appearance.

What can we see? Something that pretty much summarizes my experience being a Core Team member at CorrelAid: Lots of positivity, party vibes and love: 👍, ❤️, 🎉, 👌. The distribution is also pretty heavily skewed to the right.

![](README_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-2-1.png)

Some other observations can be made:

1.  We're huge fans of those custom emojis you can manually add to your Slack team. As such, they're of course not supported by the `emo` package and result in a number of ugly "???". Looking at their names, however, we can see the following themes emerge:

-   football/soccer: emojis bvb, b04, effzeh, sge. Basically our \#random channel.
-   culture: emojis thomas\_mann, leonardo. Even data scientists need some culture.
-   goal-orientation: emoji weltherrschaft (German for "world domination"). Speaks for itself.

1.  For being a Germany-based team, 🍻 performance is disappointing. At least, there is no 🍷 emoji at all.

2.  As data scientists, the nerd is strong with some of us. 🤖 and 🤓 are doing ok in the rankings but both are beaten by the strongest animal on earth - at least emoji-wise: the 🙈.

That's it for now. Some more fun analyses and visualizations would be:

-   who gets the most (positive/negative/overall) reactions?
-   bipartite network of users and emojis.
-   ...

Upcoming research should look into that. 😉

| emoji | emoji\_name                      |  count|
|:------|:---------------------------------|------:|
| 👍     | +1                               |    691|
| 🎉     | tada                             |    303|
| 👌     | ok\_hand                         |    170|
| ❤️    | heart                            |     86|
| 😍     | heart\_eyes                      |     39|
| 😂     | joy                              |     39|
| 💪     | muscle                           |     39|
| 😀     | grinning                         |     19|
| 😁     | grin                             |     17|
| 😄     | smile                            |     15|
| 👏     | clap                             |     13|
| ☝️    | point\_up                        |     12|
| 🙈     | see\_no\_evil                    |     12|
| 😌     | relieved                         |     11|
| 😢     | cry                              |     10|
| 🤖     | robot\_face                      |     10|
| ???   | thomas\_mann                     |     10|
| 🤔     | thinking\_face                   |      8|
| ???   | weltherrschaft                   |      8|
| 🚅     | bullettrain\_front               |      7|
| ???   | effzeh                           |      7|
| 🙂     | slightly\_smiling\_face          |      7|
| 🤓     | nerd\_face                       |      6|
| 💚     | green\_heart                     |      5|
| 😎     | sunglasses                       |      5|
| 💯     | 100                              |      4|
| 👫     | couple                           |      4|
| 🙉     | hear\_no\_evil                   |      4|
| 🍯     | honey\_pot                       |      4|
| 🐴     | horse                            |      4|
| 💁     | information\_desk\_person        |      4|
| 😭     | sob                              |      4|
| 😛     | stuck\_out\_tongue               |      4|
| 🏆     | trophy                           |      4|
| 😋     | yum                              |      4|
| 🛫     | airplane\_departure              |      3|
| 🍻     | beers                            |      3|
| 😇     | innocent                         |      3|
| 💸     | money\_with\_wings               |      3|
| 😱     | scream                           |      3|
| ???   | simple\_smile                    |      3|
| 🍝     | spaghetti                        |      3|
| 🙊     | speak\_no\_evil                  |      3|
| 😅     | sweat\_smile                     |      3|
| ☹️    | white\_frowning\_face            |      3|
| ???   | b04                              |      2|
| 😊     | blush                            |      2|
| 😵     | dizzy\_face                      |      2|
| 👻     | ghost                            |      2|
| 😬     | grimacing                        |      2|
| 😻     | heart\_eyes\_cat                 |      2|
| 🤗     | hugging\_face                    |      2|
| 😘     | kissing\_heart                   |      2|
| 🔏     | lock\_with\_ink\_pen             |      2|
| 🎓     | mortar\_board                    |      2|
| 🎥     | movie\_camera                    |      2|
| 🙆     | ok\_woman                        |      2|
| 🙌     | raised\_hands                    |      2|
| 😃     | smiley                           |      2|
| 😏     | smirk                            |      2|
| ???   | spock-hand                       |      2|
| 🦄     | unicorn\_face                    |      2|
| 🙃     | upside\_down\_face               |      2|
| ✌️    | v                                |      2|
| 👎     | -1                               |      1|
| 👶     | baby                             |      1|
| 🍺     | beer                             |      1|
| ???   | bvb                              |      1|
| 🐱     | cat                              |      1|
| 😰     | cold\_sweat                      |      1|
| 👑     | crown                            |      1|
| ???   | demokratie                       |      1|
| ✉️    | email                            |      1|
| 😑     | expressionless                   |      1|
| 👊     | facepunch                        |      1|
| ???   | fingers\_crossed                 |      1|
| ???   | flag-us                          |      1|
| 😳     | flushed                          |      1|
| ✋     | hand                             |      1|
| ⁉️    | interrobang                      |      1|
| 😆     | laughing                         |      1|
| 🍋     | lemon                            |      1|
| ???   | leonardo                         |      1|
| 📬     | mailbox\_with\_mail              |      1|
| 🌑     | new\_moon                        |      1|
| 😮     | open\_mouth                      |      1|
| 👉     | point\_right                     |      1|
| ❓     | question                         |      1|
| ???   | sge                              |      1|
| ❄️    | snowflake                        |      1|
| 😜     | stuck\_out\_tongue\_winking\_eye |      1|
| 😒     | unamused                         |      1|
| 🍉     | watermelon                       |      1|
| 👋     | wave                             |      1|
| ✅     | white\_check\_mark               |      1|
| 😉     | wink                             |      1|
| ???   | woman-heart-woman                |      1|
